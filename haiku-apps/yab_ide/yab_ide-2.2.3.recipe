SUMMARY="integrated development environment for yab."
DESCRIPTION="
Yab allows fast prototyping with simple and clean code. yab contains a large \
number of BeAPI specific commands for GUI creation and much, much more.

yab wouldn't be complete without the yab-IDE, a powerful development \
environment, which of course is programmed in yab itself.
"
HOMEPAGE="http://sourceforge.net/projects/yab-interpreter"
SRC_URI="https://github.com/HaikuArchives/Yab/archive/v1.7.3.tar.gz"
CHECKSUM_SIZE="1176449"
CHECKSUM_RMD160="f62fa02a585d9f6c5ee3eb9b781a06083352bd6f"
CHECKSUM_SHA512="a74edcf9718ee9a13a16ab0a99fbbed3cceebf1ea4bfac714b098d51531de7fdd0738e126b9a2e5d2f8d84fd0c153d9b7702e806cc91c6a97537dda11411e915"
SOURCE_DIR="Yab-1.7.3"

REVISION="1"

LICENSE="Artistic"
COPYRIGHT="1995-2006 Marc-Oliver Ihm (yabasic)
	2006-2009 Jan Bungeroth (yab improvements)
	2013 Jim Saxton (yab improvements)"

ARCHITECTURES="x86_gcc2 x86 ?x86_64"

PROVIDES="
	yab_ide$secondaryArchSuffix = $portVersion
	app:yab_IDE = $portVersion
		"

REQUIRES="
	haiku$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	yab$secondaryArchSuffix >= 1.7.02
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
"

BUILD_PREREQUIRES="
	cmd:bison
	cmd:flex
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	cmd:perl
	makefile_engine
	"

BUILD()
{
	cd src
	make $jobArgs BUILDHOME=`finddir B_SYSTEM_DEVELOP_DIRECTORY`
	unzip -o App_YAB.zip
	copyattr App_YAB yab
	cd ../yab-IDE/BuildFactory
	gcc -o yab-compress yab-compress.c -lz
}

INSTALL()
{
	mkdir -p $binDir
	mkdir -p tmp
	mkdir -p tmp/buildfactory

	cp -r src/* tmp/buildfactory
	cp -r yab-IDE/BuildFactory/* tmp/buildfactory/
	unzip -o tmp/buildfactory/parts/yabstuff.zip -d tmp/buildfactory/parts
	cp yab-IDE/src/yab-IDE.yab  tmp/buildfactory/

	pushd tmp/buildfactory
	BuildFactory.yab yab-IDE yab-IDE.yab application/x-vnd.yab-IDE
	popd

	mkdir -p $appsDir
	cp -r yab-IDE/ $appsDir/
	cp  tmp/buildfactory/yab-IDE $appsDir/yab-IDE/
	cp yab-IDE/src/yab-IDE.yab $appsDir/yab-IDE/src/

	addAppDeskbarSymlink $appsDir/yab-IDE/yab-IDE "Yab IDE"
}
