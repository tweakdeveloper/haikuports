SUMMARY="C++ unit testing framework"
DESCRIPTION="
CppUnit is a C++ unit testing framework. It started its life as a port of \
JUnit to C++ by Michael Feathers.
"
HOMEPAGE="http://cppunit.sourceforge.net/"
SRC_URI="http://downloads.sourceforge.net/cppunit/cppunit-1.12.1.tar.gz"
CHECKSUM_SHA256="ac28a04c8e6c9217d910b0ae7122832d28d9917fa668bcc9e0b8b09acb4ea44a"
LICENSE="GNU LGPL v2.1"
COPYRIGHT="
	Michael Feathers <mfeathers@objectmentor.com>
	Jerome Lacoste <lacostej@altern.org>
	E. Sommerlade <eric@sommerla.de>
	Baptiste Lepilleur <gaiacrtn@free.fr> <blep@sourceforge.net>
	Bastiaan Bakker <bastiaan.bakker@lifeline.nl>
	Steve Robbins <smr99@sourceforge.net>
	"
REVISION="1"
ARCHITECTURES="x86 x86_gcc2"

SOURCE_DIR="$portName-$portVersion"

PROVIDES="
	cppunit = $portVersion compat >= 1.12
	cmd:cppunit_config = $portVersion compat >= 1.12
	cmd:DllPlugInTester = $portVersion compat >= 1.12
	lib:libcppunit = $portVersion compat >= 1.12
	lib:libcppunit_1.12 = $portVersion compat >= 1.12
	"
REQUIRES="
	haiku
	"
BUILD_REQUIRES="
	"
BUILD_PREREQUIRES="
	haiku_devel
	cmd:libtoolize
	cmd:autoreconf
	cmd:g++
	cmd:ld
	cmd:make
	cmd:sed
	"

PATCH()
{
	# change obsolete macro
	sed -i -e 's#AM_CONFIG_HEADER(\(.*\))#AC_CONFIG_HEADERS([\1])#g' \
		configure.in
}

BUILD()
{
	libtoolize --force --copy --install
	autoreconf -i

	# there seems to be a problem with dirname
	sed -i -e 's#\/\* automatically generated \*\/#$ac_prefix_conf_INP#g' \
		configure

	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLib libcppunit
	fixPkgconfig

	packageEntries devel $developDir
}

PROVIDES_devel="
	cppunit_devel = $portVersion
	devel:libcppunit = $portVersion compat >= 1.12
	"
REQUIRES_devel="
	cppunit == $portVersion base
	"
