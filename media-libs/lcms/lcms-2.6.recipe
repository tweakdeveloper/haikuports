SUMMARY="A small-footprint, speed optimized color management engine"
DESCRIPTION="
Little CMS intends to be an open source small-footprint color management \
engine, with special focus on accuracy and performance. It uses the \
International Color Consortium standard (ICC), which is the modern standard \
when regarding to color management. The ICC specification is widely used and \
is referred to in many International and other de-facto standards. It was \
approved as an International Standard, ISO 15076-1, in 2005.
"
HOMEPAGE="http://littlecms.com/" 
SRC_URI="http://downloads.sourceforge.net/project/lcms/lcms/$portVersion/lcms2-$portVersion.tar.gz"
CHECKSUM_SHA256="5172528839647c54c3da211837225e221be93e4733f5b5e9f57668f7107e14b1"
REVISION="1"
LICENSE="MIT"
COPYRIGHT="2007 Marti Maria"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	lcms$secondaryArchSuffix = $portVersion compat >= 2
	lib:liblcms2$secondaryArchSuffix = 2.0.6 compat >= 2
	cmd:jpgicc$secondaryArchSuffix
	cmd:linkicc$secondaryArchSuffix
	cmd:psicc$secondaryArchSuffix
	cmd:tificc$secondaryArchSuffix
	cmd:transicc$secondaryArchSuffix
	"

REQUIRES="
	haiku
	lib:libjpeg$secondaryArchSuffix
	lib:libpng$secondaryArchSuffix
	lib:libtiff$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libjpeg$secondaryArchSuffix
	devel:libpng$secondaryArchSuffix
	devel:libtiff$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:gcc${secondaryArchSuffix}
	cmd:ld${secondaryArchSuffix}
	cmd:libtoolize
	cmd:aclocal
	cmd:autoconf
	cmd:make
	"
SOURCE_DIR="lcms2-$portVersion"

BUILD()
{
	libtoolize --force --copy --install
	aclocal
	automake
	autoconf

	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
	
	# prepare develop/lib
	prepareInstalledDevelLibs liblcms2
	
	fixPkgconfig
	
	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package ---------------------------------------

PROVIDES_devel="
	lcms${secondaryArchSuffix}_devel = $portVersion
	devel:liblcms2$secondaryArchSuffix = 2.0.6 compat >= 2
	"
REQUIRES_devel="
	lcms$secondaryArchSuffix == $portVersion base
	"
