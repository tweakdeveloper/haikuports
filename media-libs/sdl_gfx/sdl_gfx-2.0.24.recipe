SUMMARY="SDL graphics drawing primitives and other support functions"
DESCRIPTION="
The SDL_gfx library evolved out of the SDL_gfxPrimitives code which provided \
basic drawing routines such as lines, circles or polygons and SDL_rotozoom \
which implemented a interpolating rotozoomer for SDL_surfaces.
"
HOMEPAGE="http://www.ferzkopp.net/joomla/content/view/19/14/" 
SRC_URI="http://www.ferzkopp.net/Software/SDL_gfx-2.0/SDL_gfx-2.0.24.tar.gz"
CHECKSUM_SHA256="30ad38c3e17586e5212ce4a43955adf26463e69a24bb241f152493da28d59118"
LICENSE="Zlib"
COPYRIGHT="1999-2012 A. Schiffler"

REVISION="2"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	sdl_gfx$secondaryArchSuffix = $portVersion compat >= 2
	lib:libSDL_gfx$secondaryArchSuffix = 14.9.1 compat >= 14
	"

REQUIRES="
	haiku$secondaryArchSuffix
	libsdl$secondaryArchSuffix
	"

BUILD_REQUIRES="
	devel:libsdl$secondaryArchSuffix
	"
	
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel
	cmd:aclocal
	cmd:autoconf
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtool
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	cmd:sdl_config$secondaryArchSuffix
	"

SOURCE_DIR="SDL_gfx-$portVersion"

#BUILD_PACKAGE_ACTIVATION_PHASE = "TEST"

BUILD()
{
	./autogen.sh
	libtoolize --force --copy --install
	aclocal --force --install -I acinclude
	autoconf
	automake
	runConfigure ./configure
	make
}

INSTALL()
{
	make install

	prepareInstalledDevelLib libSDL_gfx
	fixPkgconfig

	packageEntries devel \
			$developDir
}

PROVIDES_devel="
	sdl_gfx${secondaryArchSuffix}_devel = $portVersion compat >= 2
	devel:sdl_gfx$secondaryArchSuffix = 14.9.1 compat >= 14
	devel:libSDL_gfx$secondaryArchSuffix = 14.9.1 compat >= 14
	"

REQUIRES_devel="
	sdl_gfx$secondaryArchSuffix == $portVersion base
	"

TEST()
{
	cd Test
	./autogen.sh
	runConfigure ./configure
	make
	TestABGR
	TestFonts
	TestFramerate  	
	TestImageFilter
	TestGfxBlit
	TestGfxPrimitives
	TestGfxTexture	
	TestRotozoom
	TestShrink
}
